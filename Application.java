import java.util.Scanner;


public class Application {
	
	public static void main (String [] args) {
		Scanner reader = new Scanner(System.in);
		
		Student[] section4 = new Student[3];
		
		
		for (int i=0; i<section4.length; i++) {
			
			section4[i] = new Student();
			
			System.out.println("What's your name?");
			section4[i].name = reader.nextLine();
			
			System.out.println("How many courses are you taking?");
			section4[i].numCourses = Integer.parseInt(reader.nextLine());
			
			System.out.println("How many hours of sleep do you get each day?");
			section4[i].sleepHrsPerDay = Integer.parseInt(reader.nextLine());
			
			
			//employment check
			System.out.println("Are you employed? Y/N");
			String answer = reader.nextLine();
			
			if(answer.equals("y") || answer.equals("Y")){
				section4[i].employed = true;
			
			} else if (answer.equals("n") || answer.equals("N")){
				section4[i].employed = false;
			
			} else {
				section4[i].employed = false;
				System.out.println("Invalid response. You are declared unemployed :P");
			}
			
			/*average grade
			System.out.println("What is your average grade?");
			section4[i].avgGrade = reader.nextInt();
			*/
			
		}
		
		System.out.println(section4[2].name);
		System.out.println(section4[2].numCourses);
		System.out.println(section4[2].sleepHrsPerDay);
		System.out.println(section4[2].employed);
	
	
		//below is for reference
		
		/*
		Student student1 = new Student();
		Student student2 = new Student();
		
		student1.name = "Joey";
		student1.numCourses = 8;
		student1.sleepHrsPerDay = 4;
		student1.employed = true;
		student1.avgGrade = 90;
		
		System.out.println(student1.name);
		System.out.println(student1.numCourses);
		System.out.println(student1.sleepHrsPerDay);
		System.out.println(student1.employed);
		System.out.println(student1.avgGrade);
		
		student2.name = "Noah";
		student2.numCourses = 0;
		student2.sleepHrsPerDay = 10;
		student2.employed = true;
		student2.avgGrade = 0;
		
		student1.checkOccupation();
		student2.checkOccupation();
		*/
	
	}
	
	
	
	
	
	
	
} 