
public class Student {

	//fields
	public String name;
	public int numCourses;
	public int sleepHrsPerDay;
	public boolean employed;
	public int avgGrade;

	
	public void checkOccupation(){
		
		if (numCourses == 0 && employed) {
			System.out.println("You're not a Student. You're a full-time employee!");
		}
		
		else if (numCourses > 0 && employed) {
			System.out.println("Part-time student, part-time employee.");
		}
		
		else {
			System.out.println("You're a couch potato");
		}
	}
	
	public void checkLifestyle(){
		
		if (sleepHrsPerDay <= 5 && numCourses >= 8 && avgGrade >= 90) {
			System.out.println("I genuinely don't know how you're functioning but keep going I guess?");
		
		} else if (sleepHrsPerDay < 7) {
			System.out.println("You need some sleep");
			
		} else if (avgGrade <= 60) {
			System.out.println("Maybe you should study more");
			
		} else {
			System.out.println("You're doing just fine");
			
		}
		
		
	}


}